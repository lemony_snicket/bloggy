# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#   https://github.com/stympy/faker
#   Faker::Address.city
#   Faker::Hacker.say_something_smart
#   Faker::Book.title
#   Faker::Hipster.sentences
#   Faker::Hipster.paragraph(2)
#   Faker::Superhero.name
#   Faker::ChuckNorris.fact
#   Faker::Space.nebula
#   Faker::Space.star_cluster
#   Faker::Space.constellation
#   Faker::Space.star

if false
Comment.delete_all
Message.delete_all
Post.delete_all
User.delete_all

timearray = [ 1.minute.ago, 2.minutes.ago, 3.minutes.ago, Time.zone.now, 1.hour.ago, 2.hours.ago, 1.day.ago,
              2.days.ago, 1.week.ago, 1.month.ago, 1.year.ago ]
name1 = "Rusty Shackleford"
name2 = "Stringertom"
name3 = "Dedans Penthouse"
u1 = User.create!( name: name1,
              email: "rusty@email.com",
              password: "password",
              password_confirmation: "password",
              avatar: "http://tt.tennis-warehouse.com/data/avatars/m/740/740753.jpg?1466374802",
              created_at: 1.day.ago
            )
u2 = User.create!( name: name2,
              email: "tom@email.com",
              password: "password",
              password_confirmation: "password",
              created_at: 1.minute.ago
            )
u3 = User.create!( name: name3,
              email: "dedans@example.com",
              password: "password",
              password_confirmation: "password",
              created_at: 1.week.ago
            )
admin = User.create!( name: "Sentinel",
              email: "zxcv@email.com",
              password: "password",
              password_confirmation: "password",
              avatar: "http://tt.tennis-warehouse.com/data/avatars/m/28/28953.jpg?1465412955",
              created_at: 2.weeks.ago
            )
group = [ u1, u2, u3, admin ]
puts "created users: " + User.count.to_s
names = [
"Andy Murray",
"r2473",
"Beast of Mallorca",
"Tyler Durden",
"sureshs",
"Travis Bickle",
"Indiana Jones",
"Ellen Ripley",
"The Joker",
"The Terminator",
"Dr. Carl Fairbanks",
"James Bond",
"Don Vito Corleone",
"Sweeney Todd",
"Bill 'The Butcher' Cutting",
"The Dude",
"Forrest Gump",
"Dr. Hannibal Lecter",
"Roger 'Verbal' Kint",
"Anton Chigurh",
"Vive Le",
"Glorious Mammal",
"Daniel Plainview",
"Tommy DeVito",
"Ellis Boyd 'Red' Redding",
"John McClane",
"Harry Callahan",
"Harry Potter",
"Jules Winnfield",
"Ferris Bueller",
"Tony Montana",
"Marty McFly",
"Rocky Balboa",
"Charles Foster Kane",
"Mr. Blonde",
"Jason Bourne",
"Hitman",
"Djokovic2011",
"Michael Nadal",
"Mainad",
"gut4tennis",
"BreakPoint",
"Colonel Blimp",
"LeeD",
"VeroniqueM",
"Octobrina",
"Nadal Slam King",
"90's Clay",
"Captain Jack Sparrow",
"John Coffey",
"Agent Smith",
"Lloyd Christmas",
"Bruce Wayne",
"Max Cady",
"Bob 'Bobby' Wiley",
"The Man With No Name",
"Lieutenant Colonel Frank Slade",
"Jake La Motta",
"Sonny Wortzik",
"Andy Dufresne",
"Han Solo",
"Neo",
"Norman Bates",
"Don Michael Corleone",
"Det. Alonzo Harris",
"Edward Scissorhands",
"Col. Hans Landa",
"Humphrey Bogart",
"Aragorn",
"Stanley Ipkiss",
"Howard Hughes",
"Frank Abagnale Jr.",
"Ethan Hunt",
"Raoul Duke",
"Viktor Navorski",
"Josh Baskin",
"Penny Lane",
"Arnie Grape",
"Ofcr. William M. 'Billy' Costigan Jr.",
"Derek Vinyard",
"John Keating",
"Ingrid Bergman",
"Lauren Bacall",
"Charlie Kaufman",
"Michael Dorsey",
"Phil",
"Patrick Bateman",
"Will Hunting",
"Sean Maguire",
"Freddy Krueger",
"Annie Wilkes",
"Raymond Babbitt",
"Rainman",
"Jimmy 'B-Rabbit' Smith",
"Nina Sayers",
"Max Fischer",
"Gollum",
"Leonard",
"Tony Stark",
"Gordon Gekko",
"Maximus",
"William Wallace",
"Frank Costello",
"Mickey O'Neil",
"Annie Hall",
"Alex Forrest",
"Darth Vader",
"Oskar Schindler",
"Stansfield",
"Dr. King Schultz",
"Ron Burgundy",
"Gandalf",
"Lester Burnham",
"Nostradamus",
"sureshs' Bride",
"Randle McMurphy"
]
names.each_with_index do |name, n|
  #name = Faker::Name.name
  email = "user-#{n}@example.org"
  password = "password"
  User.create!( name: name,
               email: email,
               password: password,
               password_confirmation: password,
               avatar: "http://tt.tennis-warehouse.com/styles/default/xenforo/avatars/avatar_m.png",
               created_at: timearray.sample
              )
end
puts "created all users"
puts "Users: #{User.count}"

Post.delete_all

# takes array from titles.txt 110 and allocate them to the first six users. pick user using sample
str = "
  Nice landjaeger bresaola turducken, fat sloppy cow turkey slappy hooters t-bone fatback officia alcatra veniam. Filet mignon kevin aute pastrami strychnine frankfurter bacon prosciutto capicola in occaecat eiusmod jerky non ham hock. Labore voluptate irure sunt, cupim incididunt jerky non fugiat ut swine et ut aliqua kevin. Pork belly ribeye hamburger, pork loin kielbasa spare ribs enim ut aute doner sausage sureshs poobah bartelby bovine corpulent lipidity obese fatty acid. Nostrud pork loin qui alcatra eiusmod venison rump nulla turkey dolore.
"
arr = str.split
users = User.order(:created_at).take(6)

titles = [
"Are You There, Vodka? It's Me, Chelsea",
"Midnight in the Garden of Good and Evil: A Savannah Story",
"The Perks of Being a Wallflower",
"The Man Without Qualities",
"The Earth, My Butt, and Other Big Round Things",
"Cloudy With a Chance of Meatballs",
"Me Talk Pretty One Day",
"Stop Dressing Your Six-Year-Old Like a Skank: A Slightly Tarnished Southern Belle's Words of Wisdom",
"One Hundred Years of Solitude",
"Where the Wild Things Are",
"The Long Dark Tea-Time of the Soul (Dirk Gently, #2)",
"John Dies at the End (John Dies at the End, #1)",
"A Heartbreaking Work of Staggering Genius",
"An Arsonist's Guide to Writers' Homes in New England",
"How to Lose Friends and Alienate People",
"Another Bullshit Night in Suck City",
"Alexander and the Terrible, Horrible, No Good, Very Bad Day",
"So Long, and Thanks for All the Fish (Hitchhiker's Guide to the Galaxy, #4)",
"A Clockwork Orange",
"The Gordonston Ladies Dog Walking Club",
"The Zombie Survival Guide: Complete Protection from the Living Dead",
"I Have No Mouth and I Must Scream",
"Neverwhere",
"A Thousand Splendid Suns",
"I Am America (And So Can You!)",
"Extremely Loud and Incredibly Close",
"The Devil Wears Prada",
"When You Are Engulfed in Flames",
"I Still Miss My Man But My Aim Is Getting Better",
"English as a Second F*cking Language: How to Swear Effectively, Explained in Detail With Numerous Examples Taken from Everyday Life",
"Their Eyes Were Watching God",
"The Guernsey Literary and Potato Peel Pie Society",
"The Grapes of Wrath",
"The Silence of the Lambs  (Hannibal Lecter, #2)",
"A Confederacy of Dunces",
"A Wrinkle in Time (A Wrinkle in Time Quintet, #1)",
"Go the Fuck to Sleep",
"Tequila Makes Her Clothes Fall Off (Country Music Collection, #1)",
"The Man Who Mistook His Wife for a Hat and Other Clinical Tales",
"When Will Jesus Bring the Pork Chops?",
"Fear and Loathing in Las Vegas",
"For Whom the Bell Tolls",
"Love in the Time of Cholera",
"Zen and the Art of Motorcycle Maintenance: An Inquiry Into Values",
"How to Shit in the Woods: An Environmentally Sound Approach to a Lost Art",
"If on a Winter's Night a Traveler",
"The Restaurant at the End of the Universe (Hitchhiker's Guide to the Galaxy, #2)",
"Send More Idiots",
"Brave New World",
"No Country For Old Men",
"The Man Who Was Thursday: A Nightmare",
"All Quiet on the Western Front",
"The Sound and the Fury",
"The Electric Kool-Aid Acid Test",
"The Catcher in the Rye",
"I Hope They Serve Beer in Hell (Tucker Max, #1)",
"The Spy Who Came In from the Cold",
"The Lone Ranger and Tonto Fistfight in Heaven",
"The Moon is a Harsh Mistress",
"And Then There Were None",
"What to Say When You Talk to Yourself",
"Don't Pee on My Leg and Tell Me It's Raining: America's Toughest Family Court Judge Speaks Out",
"Well-Behaved Women Seldom Make History",
"Fuck This Book",
"Shit my History Teacher DID NOT tell me!",
"Of Mice and Men",
"The Baby Jesus Butt Plug",
"The Particular Sadness of Lemon Cake",
"Nostradamus Ate My Hamster",
"As I Lay Dying",
"Lamb: The Gospel According to Biff, Christ's Childhood Pal",
"In God We Trust: All Others Pay Cash",
"She Got Up Off The Couch: And Other Heroic Acts From Mooreland, Indiana",
"Hard-Boiled Wonderland and the End of the World",
"Life, the Universe and Everything (Hitchhiker's Guide to the Galaxy, #3)",
"This Is Not a Novel",
"The Effect of Gamma Rays on Man-in-the-Moon Marigolds",
"I Capture the Castle",
"Don't Bend Over in the Garden, Granny, You Know Them Taters Got Eyes",
"On Bullshit",
"Sense and Sensibility and Sea Monsters",
"Table 21",
"Captain Underpants and the Perilous Plot of Professor Poopypants (Captain Underpants, #4)",
"Tinker, Tailor, Soldier, Spy",
"A Short History of Tractors in Ukrainian",
"How to Talk About Books You Haven't Read",
"The Importance of Being Earnest",
"Catch-22",
"Dogshit Saved My Life",
"The Lust Lizard of Melancholy Cove (Pine Cove, #2)",
"Are You There, Vodka? It's Me, Chelsea",
"Midnight in the Garden of Good and Evil: A Savannah Story",
"The Perks of Being a Wallflower",
"The Man Without Qualities",
"The Earth, My Butt, and Other Big Round Things",
"Cloudy With a Chance of Meatballs",
"Me Talk Pretty One Day",
"Stop Dressing Your Six-Year-Old Like a Skank: A Slightly Tarnished Southern Belle's Words of Wisdom",
"One Hundred Years of Solitude",
"Where the Wild Things Are",
"Do Androids Dream of Electric Sheep?",
"Something Wicked This Way Comes (Green Town, #2)",
"The Hitchhiker's Guide to the Galaxy (Hitchhiker's Guide to the Galaxy, #1)",
"Pride and Prejudice and Zombies (Pride and Prejudice and Zombies, #1)",
"I Was Told There'd Be Cake",
"The Curious Incident of the Dog in the Night-Time",
"The Hollow Chocolate Bunnies of the Apocalypse",
"Eats, Shoots & Leaves: The Zero Tolerance Approach to Punctuation",
"To Kill a Mockingbird",
"My Life as a Dog",
"The Unbearable Lightness of Being"
]
puts "about to created posts from titles"
titles.each do |title|
  user = users.sample
  body = arr.shuffle.join(" ")
  user.posts.create!(title: title, body: body, created_at: Time.zone.now)
end
puts "created posts from titles"
puts "Posts: #{Post.count}"

# then take titlesliterature and allocate using sample from entire lot

largetitles = [
"A Catskill Eagle",
"A Confederacy of Dunces",
"A Darkling Plain",
"A Fanatic Heart ",
"A Farewell to Arms",
"A Glass of Blessings",
"A Handful of Dust",
"A Many-Splendoured Thing",
"A Monstrous Regiment of Women",
"A Passage to India",
"A Scanner Darkly",
"A Shropshire Lad",
"A Summer Bird-Cage",
"A Swiftly Tilting Planet",
"A Tale of Two Cities",
"A Time of Gifts",
"A Time to Kill (Grisham novel)",
"A che punto è la notte",
"Abraham Lincoln",
"Absalom, Absalom!",
"After Many a Summer Dies the Swan",
"Ah, Wilderness!",
"All Passion Spent",
"All the King's Men",
"Alone on a Wide, Wide Sea",
"An Acceptable Time",
"An Essay on Man",
"An Evil Cradling",
"An Instant in the Wind",
"Andrew Marvell",
"Arms and the Man",
"Arthur Conan Doyle",
"As I Lay Dying ",
"Auguries of Innocence",
"Ballad of Agincourt ",
"Baruch Spinoza",
"Behold the Man ",
"Beneath the Bleeding",
"Bermudas  ",
"Beyond the Mexique Bay",
"Blithe Spirit ",
"Blood's a Rover",
"Blue Remembered Earth",
"Bonjour Tristesse",
"Book of Common Prayer",
"Book of Genesis",
"Book of Isaiah",
"Book of Jeremiah",
"Book of Judges",
"Books of Samuel",
"Brandy of the Damned ",
"Bury My Heart at Wounded Knee",
"Butter In a Lordly Dish",
"By Grand Central Station I Sat Down and Wept",
"Cabbages and Kings ",
"Carrion Comfort",
"Charles Dickens",
"Childe Roland to the Dark Tower Came",
"Clouds of Witness",
"Conrad Aiken",
"Consider Phlebas",
"Consider the Lilies ",
"Constantine P. Cavafy",
"Cover Her Face ",
"Dance Dance Dance ",
"Dante",
"David Lyndsay",
"Death Be Not Proud ",
"Death's Echo ",
"Do Not Go Gentle into That Good Night",
"Dover Beach",
"Down to a Sunless Sea",
"Dramatic Lyrics",
"Dulce et Decorum Est",
"Dying of the Light",
"East Coker",
"East of Eden ",
"Ecclesiastes",
"Ecclesiasticus",
"Ego Dominus Tuus",
"Elegy Written in a Country Churchyard",
"Elegy to the Memory of an Unfortunate Lady",
"Endless Night ",
"Epistle to the Hebrews",
"Epistle to the Romans",
"Essay on Criticism",
"Ethics (Spinoza)",
"Everything is Illuminated",
"Eyeless in Gaza ",
"Fair Stood the Wind for France",
"Fame Is the Spur ",
"Far From the Madding Crowd",
"Faust Part One",
"Fear and Trembling",
"For Whom the Bell Tolls",
"For a Breath I Tarry",
"For the Fallen",
"Frequent Hearses ",
"From Here to Eternity ",
"From Ritual to Romance",
"Gates of Damascus ",
"Gentleman Ranker",
"Gerard Manley Hopkins",
"Gerontion",
"Gettysburg Address",
"Gone with the Wind",
"Gospel of John",
"Gospel of Matthew",
"Great Work of Time",
"Hart Crane",
"Have His Carcase",
"Herman Melville",
"His Dark Materials",
"Holy Sonnets",
"Home Thoughts From Abroad",
"Horace",
"Horatian Ode upon Cromwell's Return from Ireland",
"I Corinthians",
"I Know Why the Caged Bird Sings",
"I Sing the Body Electric (Bradbury)",
"I Will Fear No Evil",
"If I Forget Thee Jerusalem",
"If Not Now, When? ",
"Il Penseroso",
"In Death Ground",
"In Dubious Battle",
"In a Dry Season",
"In a Glass Darkly",
"It's a Battlefield",
"Jacob Have I Loved",
"James Elroy Flecker",
"Jerome K. Jerome",
"Jessie L. Weston",
"Jesting Pilate (Huxley) ",
"Johann Wolfgang von Goethe",
"Kathleen Raine",
"La Belle Dame sans Merci",
"La Vita Nuova",
"Lamia ",
"Laurence Binyon",
"Leaves of Grass",
"Let Us Now Praise Famous Men",
"Lewis Carroll",
"Lift Not The Painted Veil Which Those Who Live ",
"Lilies of the Field ",
"Little Hands Clapping",
"Locksley Hall",
"Look Homeward, Angel",
"Look to Windward",
"Louis MacNeice",
"Lycidas",
"M. Scott Peck",
"Man and Superman",
"Many Waters",
"Matsuo Bashō",
"Maud ",
"Meditation XVII",
"Michael Drayton",
"Middlemarch",
"Moab Is My Washpot",
"Moby-Dick",
"Morning Song of Senlin ",
"Mother Night",
"Mr Standfast",
"Murder in the Cathedral",
"My Last Duchess",
"Nectar in a Sieve",
"Nine Coaches Waiting",
"No Country for Old Men",
"No Highway",
"No Longer at Ease",
"Noli Me Tangere ",
"Non sum qualis eram bonae sub regno Cynarae ",
"Now Sleeps the Crimson Petal",
"Now Sleeps the Crimson Petal  ",
"Number the Stars",
"O Jerusalem!",
"O Pioneers!",
"Ode on St. Cecilia's Day ",
"Ode to a Nightingale",
"Ode: Intimations of Immortality",
"Odes (Horace)",
"Of Human Bondage",
"Of Mice and Men",
"Oh! To be in England ",
"Pale Kings and Princes",
"Paradise Lost",
"Paths of Glory",
"Paul Laurence Dunbar",
"Percy Bysshe Shelley",
"Pilgrim's Progress",
"Pioneers! O Pioneers!",
"Postern of Fate",
"Precious Bane",
"Psalm 90",
"Psalms",
"Quo Vadis ",
"Qur'an",
"Recalled to Life ",
"Recalled to Life (Silverberg novel) ",
"Religio Medici",
"Remorse for Intemperate Speech",
"Rig Veda",
"Ring of Bright Water",
"Rubaiyat of Omar Khayyam",
"Rupert Brooke",
"Sailing to Byzantium",
"Samson Agonistes",
"Samuel Coleridge",
"Samuel Taylor Coleridge",
"Shall not Perish ",
"Silver Blaze",
"Sohrab and Rustum",
"Some Buried Caesar",
"Song of Songs",
"Songs of Innocence and of Experience",
"Specimen Days",
"Stranger in a Strange Land",
"Such, Such Were the Joys",
"Surprised by Joy",
"Sympathy  ",
"Taming a Sea Horse",
"Tender Is the Night",
"Terrible Swift Sword (book) ",
"That Good Night",
"That Hideous Strength",
"The Aeneid",
"The Analysis of Beauty",
"The Art of War",
"The Battle Hymn of the Republic",
"The Broken Tower",
"The Children of Men",
"The Cricket on the Hearth",
"The Curious Incident of the Dog in the Night-Time",
"The Daffodil Sky ",
"The Doors of Perception",
"The Dry Salvages",
"The Duchess of Malfi",
"The Echoing Green",
"The Far-Distant Oxus",
"The First Blast of the Trumpet Against the Monstrous Regiment of Women",
"The Glory and the Dream",
"The Golden Apples of the Sun",
"The Golden Bowl",
"The Grapes of Wrath",
"The Green Bay Tree",
"The Heart Is Deceitful Above All Things ",
"The Heart Is a Lonely Hunter",
"The House of Mirth",
"The Iliad",
"The Journey of the Magi",
"The Kingdom of God  ",
"The Lady of Shalott",
"The Last Enemy (autobiography)",
"The Last Temptation ",
"The Lathe of Heaven",
"The Line of Beauty",
"The Little Foxes",
"The Lonely Hunter",
"The Love Song of J. Alfred Prufrock",
"The Man Within",
"The Marriage of Heaven and Hell",
"The Mermaids Singing",
"The Millstone ",
"The Mirror Crack'd from Side to Side",
"The Monkey's Raincoat",
"The Moon by Night",
"The Moving Finger",
"The Moving Toyshop",
"The Needle's Eye",
"The Odyssey",
"The Other Side of Silence",
"The Painted Veil ",
"The Parliament of Man",
"The Pilgrim's Progress",
"The Proper Study",
"The Pulley ",
"The Rape of the Lock",
"The Revenger's Tragedy",
"The Rime of the Ancient Mariner",
"The Road Not Taken ",
"The Second Coming ",
"The Skull Beneath the Skin",
"The Soldier's Art",
"The Song of the Wandering Angus ",
"The Stars My Destination",
"The Stars' Tennis Balls",
"The Sun Also Rises",
"The Tables Turned",
"The Torment of Others",
"The Tyger",
"The Unbearable Lightness of Being",
"The Violent Bear It Away",
"The Walrus and the Carpenter",
"The Wanderer (1930 novel) ",
"The Waste Land",
"The Way Through the Woods",
"The Way Through the Woods  ",
"The Way of All Flesh",
"The Wealth of Nations",
"The White Devil",
"The Widening Gyre ",
"The Wife of Bath's Prologue and Tale",
"The Wind's Twelve Quarters",
"The Wings of the Dove",
"The Wives of Bath",
"The World, the Flesh and the Devil (1891 novel)",
"The Yellow Meads of Asphodel ",
"Things Fall Apart",
"This Lime Tree Bower",
"This Lime-Tree Bower My Prison",
"This Side of Paradise",
"Those Barren Leaves",
"Thoughts on Various Subjects, Moral and Diverting",
"Three Men in a Boat",
"Thrones, Dominations",
"Tiare Tahiti ",
"Tiger! Tiger! (Kipling short story)",
"Time To Murder And Create ",
"Time of our Darkness",
"Tirra Lirra by the River",
"To Sail Beyond the Sunset",
"To Say Nothing of the Dog",
"To Your Scattered Bodies Go",
"To a God Unknown",
"To a Mouse",
"To a Skylark",
"Ulysses ",
"Unweaving the Rainbow",
"Vanity Fair ",
"Vile Bodies",
"Vulgate",
"Waiting for the Barbarians",
"Waiting for the Worms ",
"What's Become of Waring",
"When the Green Woods Laugh ",
"Where Angels Fear to Tread",
"Whispers of Immortality",
"Wildfire at Midnight",
"Work Without Hope ",
"Dark Side of the Moon",
"Stairway to Heaven",
"À Peine Défigurée "
]
puts "about to created posts from largetitles"
str = "Hoodie locavore yuccie fingerstache, mixtape salvia vegan fap pickled. Actually man braid echo park, umami tumblr godard letterpress drinking vinegar. Health goth ethical fashion axe, knausgaard whatever dolore migas nulla sint offal gluten-free. Ad brunch do scenester, distillery cold-pressed banjo. Cold-pressed hammock tempor drinking vinegar, authentic affogato ad whatever. Aliquip delectus meggings, meh celiac freegan etsy quinoa irony tilde put a bird on it humblebrag. Non minim flexitarian lomo, chartreuse tofu portland actually wolf ethical 8-bit dolore delectus godard sint.Hoodie locavore yuccie fingerstache, mixtape salvia vegan fap pickled. Actually man braid echo park, umami tumblr godard letterpress drinking vinegar. Health goth ethical fashion axe, knausgaard whatever dolore migas nulla sint offal gluten-free. Ad brunch do scenester, distillery cold-pressed banjo. Cold-pressed hammock tempor drinking vinegar, authentic affogato ad whatever. Aliquip delectus meggings, meh celiac freegan etsy quinoa irony tilde put a bird on it humblebrag. Non minim flexitarian lomo, chartreuse tofu portland actually wolf ethical 8-bit dolore delectus godard sint."
arr = str.split
uc = User.count
largetitles.shuffle.each do |title|
  uoffset = rand(uc)
  user = User.offset(uoffset).first
  #user = User.sample
  body = arr.shuffle.join(" ")
  user.posts.create!(title: title, body: body, created_at: timearray.sample)
end
puts "Posts: #{Post.count}"


comments = [
  "cool story bro",
  "nice post",
  "QFT",
  "+1 ",
  "I really liked your post.",
  "Very interesting",
  "Quite insightful",
  "Total BS if you ask me",
  "You suck",
  "You are my favorite poaster",
  "ROTFLMAO",
  "You glorious mammal, just look at you!",
  "Here's looking at you, kid!",
  "Whatchoo sayin, Willis?",
  "I saw what you did there!",
  "Frankly, my dear, I don't give a hoot",
  "Toto, I've got a feeling we're not in Kansas anymore",
  "Go ahead, make my day.",
  "May the Force be with you.",
  "You talking to me?",
  "Why don't you come up sometime and see me?",
  "Is it safe?",
  "Hasta la vista, baby.",
  "No really you suck"
]
# in a loop of 100 pick a sample post and add 2 sample comments
puts "about to created comments from comments"
pc  = Post.count
uc = User.count

# Rails 4
100.times do |n|
  offset = rand(pc)
  uoffset = rand(uc)
  post = Post.offset(offset).first
  user = User.offset(uoffset).first
  post.comments.create!(body: comments.sample, user_id: user.id)
  user = User.offset(uoffset).first
  post.comments.create!(body: comments.sample, user_id: user.id)
end
puts "Comments: #{Comment.count}"

quotes = [
"Frankly, my dear, I don't give a damn.",
"I'm going to make him an offer he can't refuse.",
"You don't understand!  I coulda had class. I coulda been a contender. I could've been somebody, instead of a bum, which is what I am.",
"Toto, I've got a feeling we're not in Kansas anymore.",
"Here's looking at you, kid.",
"Go ahead, make my day.",
"All right, Mr. DeMille, I'm ready for my close-up.",
"May the Force be with you.",
"Fasten your seatbelts. It's going to be a bumpy night.",
"You talking to me?",
"What we've got here is failure to communicate.",
"I love the smell of napalm in the morning.  ",
"Love means never having to say you're sorry.",
"The stuff that dreams are made of.",
"E.T. phone home.",
"They call me Mister Tibbs!",
"Rosebud.",
"Made it, Ma! Top of the world!",
"I'm as mad as hell, and I'm not going to take this anymore!",
"Louis, I think this is the beginning of a beautiful friendship.",
"A census taker once tried to test me. I ate his liver with some fava beans and a nice Chianti.",
"Bond. James Bond.",
"There's no place like home.  ",
"I am big!  It's the pictures that got small.",
"Show me the money!",
"Why don't you come up sometime and see me?",
"I'm walking here!  I'm walking here!",
"Play it, Sam.  Play 'As Time Goes By.'",
"You can't handle the truth!",
"I want to be alone.",
"After all, tomorrow is another day!",
"Round up the usual suspects.",
"I'll have what she's having.",
"You know how to whistle, don't you, Steve? You just put your lips together and blow.",
"You're gonna need a bigger boat.",
"Badges? We ain't got no badges! We don't need no badges! I don't have to show you any stinking badges!",
"I'll be back.",
"Today, I consider myself the luckiest man on the face of the earth.",
"If you build it, he will come. ",
"Mama always said life was like a box of chocolates. You never know what you're gonna get.",
"We rob banks.",
"Plastics.",
"We'll always have Paris.",
"I see dead people.",
"Stella!  Hey, Stella!",
"Oh, Jerry, don't let's ask for the moon. We have the stars.",
"Shane.  Shane.  Come back!",
"Well, nobody's perfect.",
"It's alive!  It's alive!",
"Houston, we have a problem.",
"You've got to ask yourself one question: 'Do I feel lucky?' Well, do ya, punk?",
"You had me at hello.",
"One morning I shot an elephant in my pajamas. How he got in my pajamas, I don't know.",
"There's no crying in baseball!",
"La-dee-da, la-dee-da.",
"A boy's best friend is his mother.",
"Greed, for lack of a better word, is good.",
"Keep your friends close, but your enemies closer.",
"As God is my witness, I'll never be hungry again.",
"Well, here's another nice mess you've gotten me into!",
"Say hello to my little friend!",
"What a dump.",
"Mrs. Robinson, you're trying to seduce me.  Aren't you?",
"Gentlemen, you can't fight in here! This is the War Room!",
"Elementary, my dear Watson.",
"Get your stinking paws off me, you damned dirty ape.",
"Of all the gin joints in all the towns in all the world, she walks into mine.",
"Here's Johnny!",
"They're here!",
"Is it safe?",
"Wait a minute, wait a minute.  You ain't heard nothin' yet!",
"No wire hangers, ever!",
"Mother of mercy, is this the end of Rico?",
"Forget it, Jake, it's Chinatown.",
"I have always depended on the kindness of strangers.",
"Hasta la vista, baby.",
"Soylent Green is people!",
"Open the pod bay doors, HAL.",
"Striker: Surely you can't be serious.  ",
"Rumack: I am serious and don't call me Shirley.",
"Yo, Adrian!",
"Hello, gorgeous.",
"Toga!  Toga!",
"Listen to them.  Children of the night.  What music they make.",
"Oh, no, it wasn't the airplanes. It was Beauty killed the Beast.",
"My precious.",
"Attica! Attica!",
"Sawyer, you're going out a youngster, but you've got to come back a star!",
"Listen to me, mister. You're my knight in shining armor.  Don't you forget it.  You're going to get back on that horse, and I'm going to be right behind you, holding on tight, and away we're gonna go, go, go!",
"Tell 'em to go out there with all they got and win just one for the Gipper.",
"A martini.  Shaken, not stirred.",
"Who's on first.",
"Cinderella story.  Outta nowhere.  A former greenskeeper, now, about to  become the Masters champion.  It looks like a mirac...It's in the hole!  It's in the hole!  It's in the hole!",
"Life is a banquet, and most poor suckers are starving to death!",
"I feel the need - the need for speed!",
"Carpe diem.  Seize the day, boys.  Make your lives extraordinary.",
"Snap out of it!",
"My mother thanks you. My father thanks you. My sister thanks you. And I thank you.",
"Nobody puts Baby in a corner.",
"I'll get you, my pretty, and your little dog, too!",
"I'm king of the world!"
]
puts "about to created posts from quotes"
quotes.each do |body|
  user = users.sample
  title = titles.sample
  post = user.posts.create!(title: title, body: body, created_at: timearray.sample)
  post.comments.create!(body: "Who said this?", user_id: u1.id)
  post.comments.create!(body: quotes.sample, user_id: u2.id)
end
puts "created posts from quotes"
puts "Posts: #{Post.count}"
puts "about to create messages from quotes"
quotes.each do |body|
  _u1, _u2 = group.sample(2)
  #title = titles.sample
  #post = user.posts.create!(title: title, body: body, created_at: timearray.sample)
  #_u1.comments.create!(body: "Who said this?", user_id: u1.id)
  _u1.messages.create!(body: body, sender_id: _u2.id)
end
puts "created messages from quotes"
u1.messages.create!(body: "The Unbearable Tightness of Suresh", sender_id: admin.id)
u1.messages.create!(body: "Gone with the Wind (hopefully not Suresh!)", sender_id: u2.id)
puts "Messages: #{Message.count}"
puts "DONE"
admin.posts.create!(title: "Gone with the Wind (hopefully not suresh)", body: "Frankly my dear, I don't give a ", created_at: 10.seconds.ago)
admin.posts.create!(title: "He Got Up Off The Couch: And Other Heroic Acts From San Deigo, CA", body: "Like Gilbert Grape, only Heavier! ", created_at: 5.seconds.ago)

body="Hello, my name is Crusty Cackleturd. <p>My turn ons are bubble wrap, pepperoni sandwiches, and girls with eyepatches. My turn offs are unicellular organisms, the Cartesian Plane, and Sumatran Pimple.<p> I like this site very much, and I hope to meat some one eyed girls (no Sumatran Pimple, please). <br>Ok thks bai."
admin.messages.create!(body: body, sender_id: u1.id)
body="Hello, my name is Crusty Cackleturd. My turn ons are bubble wrap, pepperoni sandwiches, and girls with eyepatches. My turn offs are unicellular organisms, the Cartesian Plane, and Sumatran Pimple. <br>I like this site very much, and I hope to meat some one eyed girls (no Sumatran Pimple, please). Ok thks bai. <p>Oh, I'm also very lazy. One time some toilet paper got trapped in my shorts, and I walked around all day like dat because I didn't feel like taking my pants off. That's why I pasted this from my profile page. <p>May Sushi San be with you, Crusty "
title = "Eentroduckshun"
post = u1.posts.create!(title: title, body: body, created_at: Time.zone.now)
post.comments.create!(body: "Welcome to Troll Warehouse, Crusty", user_id: admin.id)
#@post.comments.create!(body: "another comment by someone", user_id: u3.id)


users = User.all
users.each do |user|
  user.location = Faker::Address.city
  user.signature = Faker::Hacker.say_something_smart
  user.dob = Faker::Date.between(60.years.ago, 20.years.ago)
  user.save
end
end
unless u1
  u1 = User.find_by(email: 'rusty@email.com')
end
u1.signature = "My greatest satisfactions these days consist of various physiological functions involving the secretion of different substances."
u1.location = "Between Sureshsina Flatulentova's XXL Sized Thighs"
u1.save
puts "Done !"
