class AddSenderToMessages < ActiveRecord::Migration
  def change
    add_column :messages, :sender_id, :integer
    add_column :messages, :receiver_id, :integer
    #add_reference :messages, :user, index: true, foreign_key: :sender_id, primary_key: :user_id
    #add_reference :messages, :user, index: true, foreign_key: :receiver_id, primary_key: :user_id
    add_foreign_key :messages, :users, column: :sender_id, primary_key: :id
    add_foreign_key :messages, :users, column: :receiver_id, primary_key: :id
  end
end
