class AddColumnsToUser < ActiveRecord::Migration
  def change
    add_column :users, :location, :string
    add_column :users, :dob, :datetime
    add_column :users, :sex, :varchar
    add_column :users, :signature, :text
  end
end
