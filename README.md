# README

This is based on week two of Mackenzie child's videos
it is a simple blog.

I am going a bit further by creating a link between user and post, and user and comment, too.

This is on heroku as : https://thawing-dusk-43757.herokuapp.com/

- 2016-06-21 - Have added the ability to leave messages on someone else's profile.

Issues:

- rich editing with ckeditor-rails is not functioning. It does not show up at all on posts.
