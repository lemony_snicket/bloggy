class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :posts,    dependent:   :destroy
  # XXX should the next have been through: posts  ... but they are not his posts
  has_many :comments, dependent:   :destroy
  # messages that the user sends to other users
  has_many :sent_messages, class_name: 'Message', 
                      foreign_key: 'sender_id',
                      dependent:   :destroy
  # messages that the user receives from other users
  has_many :messages, class_name: 'Message', 
                      foreign_key: 'receiver_id',
                      dependent:   :destroy
end
