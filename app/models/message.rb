class Message < ActiveRecord::Base
  # in the next two, the foriegn_key is determined by rails to be sender_id amd receiver_id
  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'
  validates :sender_id, presence: true
  validates :receiver_id, presence: true
  validates :body, presence: true
end
