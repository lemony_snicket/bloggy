class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]

  def index
    #@posts = Post.all.order("created_at DESC")
    @posts = Post.all.order("created_at DESC").paginate(page: params[:page])
  end
  def new
    @post = current_user.posts.new
    #@post = current_user.posts.new
  end
  def create
    #@post = Post.new(post_params)
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to root_url
    else
      render 'new'
    end
  end
  def edit
    @post = Post.find(params[:id])
  end

  def show
    @post = Post.find(params[:id])
  end
  def update
    @post = Post.find(params[:id])
    if @post.update(params[:post].permit(:title, :body))
      redirect_to @post
    else
      render 'edit'
    end
  end
  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    redirect_to posts_path
  end

	private

	def post_params
		params.require(:post).permit(:title, :body)
	end
end
