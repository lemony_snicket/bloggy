class UsersController < ApplicationController
  
  # display a list of users paginated
  def index
    @users = User.all.order(:created_at).paginate( page: params[:page] )
  end
  def edit
    @user = User.find(params[:id])
  end

  def update
    #@user = User.new(user_params)
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # handle a successful update
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  # profile page of user. user_path or link_to current_user
  def show
    @user = User.find(params[:id])
    # prevent viewing of user that is not activated
    #redirect_to root_url and return unless @user.activated?
    #@microposts = @user.microposts.paginate( page: params[:page] )
    @comments = @user.comments.order("created_at DESC").take(10)
    @comment_count = @user.comments.count
    @posts = @user.posts.order("created_at DESC").take(10)
    @post_count = @user.posts.count
    # last 10 messages
    @messages = @user.messages.order("created_at DESC").take(10)
    @message_count = @user.posts.count
  end
  # all comments of the user
  def comments
    @user = User.find(params[:id])
    #@comments = Comment.where("user_id = ?", @user.id).order("created_at DESC")
    @comments = @user.comments.order("created_at DESC")
  end
  # all posts of the user
  def posts
    @user = User.find(params[:id])
    @posts = @user.posts.order("created_at DESC")
  end
  # all messages of the user
  # XXX these 3 should be paginated
  def posts
    @user = User.find(params[:id])
    @messages = @user.messages.order("created_at DESC")
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :sex, :location, :avatar, :signature, :dob)
    end
end
