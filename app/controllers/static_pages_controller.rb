class StaticPagesController < ApplicationController
  def home

    # post and comment already have a default scope so they are ordered on created_at DESC
    @new_posts = Post.limit(10)
    @new_comments = Comment.limit(10)
    # http://stackoverflow.com/questions/2919720/how-to-get-record-created-today-by-rails-activerecord
    @new_users = User.where("created_at >= ?", Time.zone.now.beginning_of_day)
    #add_index :posts, :created_at
  end

  def help
  end

  def about
  end

  def contact_us
  end
end
