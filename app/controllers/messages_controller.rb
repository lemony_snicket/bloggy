class MessagesController < ApplicationController
  def create
    #$stderr.puts "PARAMS::: #{params[:message].keys}"
    id = params[:message][:receiver_id]
    @user = User.find(id)
    # I need to allow user_id here too since i am updating it as a hidden field.
    # Or else I could have added it here ??
    @message = @user.messages.create(params[:message].permit(:body, :sender_id, :receiver_id))

    redirect_to user_path(@user)
  end

  def destroy
  end
end
