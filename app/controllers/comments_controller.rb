class CommentsController < ApplicationController
  def create
    @post = Post.find(params[:post_id])
    # I need to allow user_id here too since i am updating it as a hidden field.
    # Or else I could have added it here ??
    @comment = @post.comments.create(params[:comment].permit(:name, :body, :user_id))

    redirect_to post_path(@post)
  end
  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(param[:id])
    @comment.destroy
    redirect_to post_path(@post)
  end
end
