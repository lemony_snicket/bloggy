Rails.application.routes.draw do
  get 'messages/create'

  get 'messages/destroy'

  get 'static_pages/home'

  get 'static_pages/help'

  get 'static_pages/about'

  get 'static_pages/contact_us'

  devise_for :users
  resources :posts do
      resources :comments, shallow: true
  end
  root "static_pages#home"

  get 'help'    => 'static_pages#help'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  # The next two can be put as members of users
  #get 'users/:id/posts/', to: 'users#user_posts', as: 'users_posts'
  #get 'users/:id/comments', to: 'users#user_comments', as: 'users_comments'
  # so we can see users profile 
  resources  :users do
    get 'posts', on: :member
    get 'comments', on: :member
    get 'messages', on: :member
    #resources :messages, shallow: true
  end
  resources :messages

end
